// Initializes the `attributions` service on path `/attributions`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Attributions } from './attributions.class';
import createModel from '../../models/attributions.model';
import hooks from './attributions.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'attributions': Attributions & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/attributions', new Attributions(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('attributions');

  service.hooks(hooks);
}
