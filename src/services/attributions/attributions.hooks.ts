import { disallow } from 'feathers-hooks-common';

// Read only service, writable only from other services.

export default {
  before: {
    all: [],
    find: [],
    get: [disallow()],
    create: [disallow('external')],
    update: [disallow()],
    patch: [disallow('external')],
    remove: [disallow()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
