import { Application } from '../declarations';
import attributions from './attributions/attributions.service';
import events from './events/events.service';
import pageviews from './pageviews/pageviews.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(attributions);
  app.configure(events);
  app.configure(pageviews);
}
