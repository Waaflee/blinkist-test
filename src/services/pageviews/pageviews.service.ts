// Initializes the `pageviews` service on path `/pageviews`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Pageviews } from './pageviews.class';
import hooks from './pageviews.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'pageviews': Pageviews & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/pageviews', new Pageviews(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('pageviews');

  service.hooks(hooks);
}
