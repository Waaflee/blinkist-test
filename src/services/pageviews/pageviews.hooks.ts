import { BadRequest } from '@feathersjs/errors';
import { HookContext } from '@feathersjs/feathers';
import { disallow } from 'feathers-hooks-common';

const createPageView = async (context: HookContext) => {
  const { app, data } = context;
  if (data) {
    const result = await app.service('attributions').create(data);
    context.result = result;
  } else {
    throw new BadRequest("Request body can't be empty");
  }

  return context;
};

// we could add a validation step before processing data with a validation schema such as ZOD.JS
// write-only service.

export default {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [createPageView],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
