import { PageEvent, PageView } from './../../models/attributions.model';
import { BadRequest, NotFound } from '@feathersjs/errors';
import { disallow } from 'feathers-hooks-common';
import { HookContext } from '@feathersjs/feathers';
import { Paginated } from '@feathersjs/feathers';

const createEvent = async (context: HookContext<PageEvent>) => {
  const { app, data } = context;
  // console.log({ data });
  if (data) {
    const pageviewQueryResult = (await app.service('attributions').find({
      query: {
        fingerprint: data.fingerprint,
        created_at: { $lte: new Date(data.created_at).getTime() },
        $sort: { created_at: -1 },
        $limit: 1,
      },
    })) as Paginated<PageView>;

    const pageview =
      pageviewQueryResult.total === 1 && pageviewQueryResult.data[0];

    console.log(pageviewQueryResult);

    if (!pageview) {
      throw new NotFound(
        'pageview for fingerprint: ' + data.fingerprint + ' not found!'
      );
    }

    const result = await app
      .service('attributions')
      .patch(pageview._id, { ...pageview, user_id: data.user_id, event: data });
    context.result = result;
  } else {
    throw new BadRequest("Request body can't be empty");
  }

  return context;
};

// we could add a validation step before processing data with a validation schema such as ZOD.JS
// write-only service.

export default {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [createEvent],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
