// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const where = Object.assign({}, context.params.query);

    const transformQuery = (obj: any) => {
      Object.keys(obj).forEach(function (prop) {
        const value = obj[prop];
        if (value !== null && typeof value === 'object')
          obj[prop] = transformQuery(value);
        else if (typeof value === 'string' && value.toLowerCase() === 'null')
          obj[prop] = null;
      });
      return obj;
    };

    context.params.query = transformQuery(where);
    return context;
  };
};
