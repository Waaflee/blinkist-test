// attributions-model.ts - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
import { Application } from '../declarations';
import { Model, Mongoose } from 'mongoose';

export default function (app: Application): Model<any> {
  const modelName = 'attributions';
  const mongooseClient: Mongoose = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const schema = new Schema(
    {
      fingerprint: { type: String, required: true },
      user_id: { type: String, required: false },
      url: { type: String, required: true },
      referrer_url: { type: String, required: false },
      created_at: { type: Date, required: true, default: Date.now },
      event: {
        type: {
          name: { type: String, required: true },
          // fingerprint: { type: String, required: true }, not needed since we already have it.
          // user_id: { type: String, required: true }, also not needed.
          created_at: { type: Date, required: true },
        },
        required: false,
      },
    },
    {
      timestamps: false,
    }
  );
  // This is necessary to avoid model compilation errors in watch mode
  // see https://mongoosejs.com/docs/api/connection.html#connection_Connection-deleteModel
  if (mongooseClient.modelNames().includes(modelName)) {
    (mongooseClient as any).deleteModel(modelName);
  }
  return mongooseClient.model<any>(modelName, schema);
}

type DateISOString = string;

export interface PageEvent {
  name: 'signup';
  fingerprint: string;
  user_id: string;
  created_at: DateISOString;
}
export interface PageView {
  _id: string | number;
  text: string;
  fingerprint: string;
  user_id?: string;
  url: string;
  referrer_url?: string;
  created_at: DateISOString;
  event?: PageEvent;
}
