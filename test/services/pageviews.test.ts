import app from '../../src/app';

describe('\'pageviews\' service', () => {
  it('registered the service', () => {
    const service = app.service('pageviews');
    expect(service).toBeTruthy();
  });
});
