# Blinklist Attributions Microservice

> Data analytics microservice

## Description

there are 3 endpoints

- `/pageviews` write-only endpoint receives pageviews
- `/events` write-only endpoint receives events
- `/attributions` read-only endpoint, provides data.

Data is stored in mongodb following the schema present in src/models/attributions.model.ts

the attributions endpoint can be queries using the following API: [Querying](https://crow.docs.feathersjs.com/api/databases/querying.html#equality).

For example, to count how many pageviews with a referrer url from a facebook add resulted in a signup (conversions):

`http://localhost:3030/attributions?referrer_url=https://www.facebook.com/&$limit=0&event[$ne]=null`

Here we are asking the amount of conversions from facebooks adds:

"count" ($limit=0) every registry with a "referrer_url" equal to https://www.facebook.com/ (facebook add as per given documentation) AND which attached event isn't NULL

To just get the number of available records we set $limit to 0. This will only run a (fast) counting query against the database and return a page object with the total and an empty data array. If we omit this we would also get the matching documents.

the response follow the next structure:

```
{
    "total": number, //amount of matching registries
    "limit": number, // elements per page (pagination)
    "skip": number, // skipped elements (pagination)
    "data": Array of data ([document1, document2, ...])
}
```

# Feathers

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

   ```
   cd path/to/blinklist
   npm install
   ```

3. Start your app

   ```
   npm start
   ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers help                           # Show all commands
```

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).
