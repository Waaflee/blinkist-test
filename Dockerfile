FROM node:19-alpine

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install

# here we should compile and have a multi stage build so we can reduce final container size
# RUN yarn compile

COPY . .

EXPOSE 3030

CMD ["yarn", "dev"]
# if we compile we can run in production mode, now we leave it in development so we can get better loggin
# CMD ["node", "lib/"]
